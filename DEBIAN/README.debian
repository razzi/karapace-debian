# karapace-debian

This branches off the Karapace source code to add debian
package building using https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/.

This repository was created as follows:

- clone the upstream karapace source (`git clone git@github.com:aiven/karapace.git`)
- pull tags (`git fetch --tags`)
- checkout the most recent tag (`git switch --detach 2.1.3`)
- create a debian branch (`git switch --create debian`)

Then files for the debian packaging go in the DEBIAN directory.

To build the debian package, there are 2 steps
- building the conda environment into a .tar.gz using docker
- packaging the .tar.gz into a .deb file on a build server

The build process is using a temporary fork of workflow_utils, since git is needed by
karapace to pull its python dependencies.

# Build the conda environment

```sh
docker build -t workflow_utils:conda_dist -f DEBIAN/extra/Dockerfile.conda-dist .

docker run --mount type=bind,source=$(pwd),target=/srv/project workflow_utils:conda_dist
```

# Package .tar.gz into .deb

This process is hacky and treats the package as a binary.
The whole dependency should be removed from datahub eventually,
and we won't need this code any more.

See https://feature-requests.datahubproject.io/b/Developer-Experience/p/remove-required-dependency-on-confluent-schema-registry

```sh
rsync --archive --progress DEBIAN deneb.codfw.wmnet:karapace-debian/

# Replace the timestamp in the filename below
scp conda_dist_env.2022-03-15T15.56.43.tgz deneb.codfw.wmnet:

ssh deneb.codfw.wmnet

# If you have run the build process before, remove these directories first
mkdir -p karapace-dist karapace-debian-tree/usr/lib/karapace

tar -zxf conda_dist_env.2022-03-15T19.34.31.tgz -C karapace-debian-tree/usr/lib/karapace

mkdir karapace-debian-tree/DEBIAN
cp karapace-debian/DEBIAN/control karapace-debian-tree/DEBIAN

# Put the copyright/changelog metadata into the distribution
cp -r karapace-debian/DEBIAN/extra/usr/share/doc/ karapace-debian-tree/usr/share/

fakeroot dpkg-deb --build karapace-debian-tree karapace-dist
```

The .deb file will be written to `karapace-dist/`.

# Upload the .deb to apt.wikimedia.org

Close the ssh connection and copy the files onto your laptop:

```sh
scp -r deneb.codfw.wmnet:karapace-dist /tmp/

scp -r /tmp/karapace-dist/ apt1001.wikimedia.org:

ssh apt1001.wikimedia.org

# Become root
sudo -i

# Change this to the version you're uploading
reprepro -C main includedeb bullseye-wikimedia /home/razzi/karapace-dist/karapace_2.1.3-py3.7-1_amd64.deb
```
